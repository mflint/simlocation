//
//  MainView.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-20.
//

import SwiftUI

struct MainView: View {
	@StateObject var viewModel = MainViewModel()

	var body: some View {
		NavigationSplitView {
			SavedLocationsListView(viewModel: self.viewModel)
		} detail: {
			MapView(viewModel: self.viewModel)
		}
		.onAppear {
			self.viewModel.onAppear()
		}
		.navigationTitle("Sim Location")
		.toolbar {
			ToolbarItem(placement: .primaryAction) {
				Toggle(isOn: self.$viewModel.jitter) {
					Text("Jitter")
				}
				.toggleStyle(CheckboxToggleStyle())
				.keyboardShortcut(.init("j", modifiers: .command))
				.help("⌘J")
			}

			ToolbarItem(placement: .primaryAction) {
				Button {
					self.viewModel.save()
				} label: {
					Text("Save location")
				}
				.keyboardShortcut(.init("s", modifiers: .command))
				.help("⌘S")
			}

			ToolbarItem(placement: .primaryAction) {
				Button {
					self.viewModel.findSims()
				} label: {
					Text("Find sims")
				}
				.keyboardShortcut(.init("f", modifiers: .command))
				.help("⌘F")
			}
		}
	}
}

struct MainView_Previews: PreviewProvider {
	static var previews: some View {
		MainView()
	}
}
