//
//  KeyStroke.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-22.
//

import SwiftUI
import AppKit

private extension CGKeyCode {
	// see https://gist.github.com/chipjarred/cbb324c797aec865918a8045c4b51d14
	static let kVK_Delete: CGKeyCode = 0x33

	static let kVK_Command                   : CGKeyCode = 0x37
	static let kVK_Shift                     : CGKeyCode = 0x38
	static let kVK_Option                    : CGKeyCode = 0x3A
	static let kVK_Control                   : CGKeyCode = 0x3B

	static let modifiers = [
		kVK_Command,
		kVK_Shift,
		kVK_Option,
		kVK_Control,
	]

	static let eventModifiersMap: [CGKeyCode : EventModifiers] = [
		.kVK_Command : EventModifiers.command,
		.kVK_Shift : EventModifiers.shift,
		.kVK_Option : EventModifiers.option,
		.kVK_Control : EventModifiers.control,
	]

	var isPressed: Bool {
		CGEventSource.keyState(.combinedSessionState, key: self)
	}
}

/// convert SwiftUI KeyEquivalent to CGKeyCode
private extension KeyEquivalent {
	private static let equivalentMap: [Character : CGKeyCode] = [
		KeyEquivalent.delete.character : .kVK_Delete,
	]

	func cgKeyCode() -> CGKeyCode? {
		Self.equivalentMap[self.character]
	}
}

extension View {
	/// Defines a key combination that performs an action, which isn't associated with a Button or Toggle
	func keyStroke(key: KeyEquivalent,
				   modifiers: EventModifiers = .command,
				   action: @escaping () -> Void) -> some View {
		self
			.overlay {
				KeyStrokeView(key,
							  modifiers: modifiers,
							  action: action)
			}
	}
}

private struct KeyStrokeView: NSViewRepresentable {
	private let keyCode: CGKeyCode
	private let modifiers: [CGKeyCode]
	private let noModifiers: [CGKeyCode]
	private let action: () -> Void

	init(_ key: KeyEquivalent,
		 modifiers eventModifiers: EventModifiers,
		 action: @escaping () -> Void) {
		// convert SwiftUI KeyEquivalent to CGKeyCode
		guard let keyCode = key.cgKeyCode() else {
			preconditionFailure("missing CGKeyCode for \(key)")
		}
		self.keyCode = keyCode

		// convert SwiftUI EventModifiers to an array of CGKeyCode
		var modifiers = [CGKeyCode]()
		var noMdifiers = [CGKeyCode]()
		for cgKeyCode in CGKeyCode.modifiers {
			guard let eventModifier = CGKeyCode.eventModifiersMap[cgKeyCode] else {
				preconditionFailure("missing modifier")
			}
			if eventModifiers.contains(eventModifier) {
				modifiers.append(cgKeyCode)
			} else {
				noMdifiers.append(cgKeyCode)
			}
		}

		self.modifiers = modifiers
		self.noModifiers = noMdifiers
		self.action = action
	}

	func makeNSView(context: Context) -> KeyboardShortcutView {
		KeyboardShortcutView(keyCode: self.keyCode,
							 modifiers: self.modifiers,
							 noModifiers: self.noModifiers,
							 action: self.action)
	}

	func updateNSView(_ nsView: KeyboardShortcutView, context: Context) {}
}

private class KeyboardShortcutView: NSView {
	private let keyCode: CGKeyCode
	private let modifiers: [CGKeyCode]
	private let noModifiers: [CGKeyCode]
	private let action: () -> Void

	init(keyCode: CGKeyCode,
		 modifiers: [CGKeyCode],
		 noModifiers: [CGKeyCode],
		 action: @escaping () -> Void) {
		self.keyCode = keyCode
		self.modifiers = modifiers
		self.noModifiers = noModifiers
		self.action = action

		super.init(frame: .zero)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func performKeyEquivalent(with event: NSEvent) -> Bool {
		if event.type == .keyDown &&
			event.keyCode == self.keyCode &&
			self.modifiers.allSatisfy({ $0.isPressed }) &&
			self.noModifiers.allSatisfy({ !$0.isPressed }) {
			self.action()
			return true
		}

		return false
	}
}
