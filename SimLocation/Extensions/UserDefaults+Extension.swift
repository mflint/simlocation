//
//  UserDefaults+Extension.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-21.
//

import Foundation
import MapKit

extension UserDefaults {
	private static let regionKey = "region"
	private static let locationsKey = "locations"
	private static let jitterKey = "jitter"

	static func registerSimLocationDefaults() {
		UserDefaults.standard.register(defaults: [
			Self.regionKey : try! MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 54.5, longitude: -3),
													span: MKCoordinateSpan(latitudeDelta: 6, longitudeDelta: 6))
			.toData(),
			Self.locationsKey : try! SavedLocations(locations: []).toData()
		])
	}

	var region: MKCoordinateRegion {
		get {
			let data = self.data(forKey: Self.regionKey)
			return try! MKCoordinateRegion(data: data)!
		}
		set {
			try? self.setValue(newValue.toData(), forKey: Self.regionKey)
		}
	}

	var locations: SavedLocations {
		get {
			let data = self.data(forKey: Self.locationsKey)
			return try! SavedLocations(data: data)!
		}
		set {
			try? self.setValue(newValue.toData(), forKey: Self.locationsKey)
		}
	}

	var jitter: Bool {
		get {
			return self.bool(forKey: Self.jitterKey)
		}
		set {
			self.setValue(newValue, forKey: Self.jitterKey)
		}
	}
}
