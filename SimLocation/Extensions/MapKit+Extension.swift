//
//  MapKit+Extension.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-21.
//

import MapKit

extension MKCoordinateRegion: Equatable {
	public static func == (lhs: MKCoordinateRegion, rhs: MKCoordinateRegion) -> Bool {
		lhs.center == rhs.center && lhs.span == rhs.span
	}
}

extension MKCoordinateSpan: Equatable {
	public static func == (lhs: MKCoordinateSpan, rhs: MKCoordinateSpan) -> Bool {
		lhs.latitudeDelta == rhs.latitudeDelta && lhs.longitudeDelta == rhs.longitudeDelta
	}
}

extension MKCoordinateRegion: Codable {
	private enum CodingKeys: String, CodingKey {
		case latitude
		case longitude
		case latitudeDelta
		case longitudeDelta
	}

	public init(from decoder: Decoder) throws {
		self.init()

		let values = try decoder.container(keyedBy: CodingKeys.self)
		self.center = try CLLocationCoordinate2D(latitude: values.decode(CLLocationDegrees.self, forKey: .latitude),
												 longitude: values.decode(CLLocationDegrees.self, forKey: .longitude))
		self.span = try MKCoordinateSpan(latitudeDelta: values.decode(CLLocationDegrees.self, forKey: .latitudeDelta),
										 longitudeDelta: values.decode(CLLocationDegrees.self, forKey: .longitudeDelta))
	}

	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(self.center.latitude, forKey: .latitude)
		try container.encode(self.center.longitude, forKey: .longitude)
		try container.encode(self.span.latitudeDelta, forKey: .latitudeDelta)
		try container.encode(self.span.longitudeDelta, forKey: .longitudeDelta)
	}
}

extension MKCoordinateRegion {
	init?(data: Data?) throws {
		guard let data else { return nil }
		self = try JSONDecoder().decode(Self.self, from: data)
	}

	func toData() throws -> Data {
		try JSONEncoder().encode(self)
	}
}
