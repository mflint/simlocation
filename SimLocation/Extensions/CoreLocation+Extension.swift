//
//  CoreLocation+Extension.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-21.
//

import CoreLocation

extension CLLocationCoordinate2D: Equatable {
	public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
		lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
	}
}

extension CLLocationCoordinate2D: Codable {
	private enum CodingKeys: String, CodingKey {
		case latitude
		case longitude
	}
	
	public init(from decoder: Decoder) throws {
		self.init()
		
		let values = try decoder.container(keyedBy: CodingKeys.self)
		self = try CLLocationCoordinate2D(latitude: values.decode(CLLocationDegrees.self, forKey: .latitude),
										  longitude: values.decode(CLLocationDegrees.self, forKey: .longitude))
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(self.latitude, forKey: .latitude)
		try container.encode(self.longitude, forKey: .longitude)
	}
}

extension CLLocationCoordinate2D {
	// https://stackoverflow.com/a/16850439/408142
	func adjusted(distance: Double, bearingRadians: Double) -> CLLocationCoordinate2D {
		let distanceRadians = distance / 6_371_000  // 6,371,000 == Earth's radius in m
		let fromLatRadians = self.latitude * .pi / 180
		let fromLonRadians = self.longitude * .pi / 180

		let toLatRadians = asin(sin(fromLatRadians) * cos(distanceRadians) + cos(fromLatRadians) * sin(distanceRadians) * cos(bearingRadians))

		var toLonRadians = fromLonRadians + atan2(sin(bearingRadians)
												  * sin(distanceRadians) * cos(fromLatRadians), cos(distanceRadians)
												  - sin(fromLatRadians) * sin(toLatRadians))

		// adjust toLonRadians to be in the range -180 to +180...
		toLonRadians = fmod((toLonRadians + 3 * .pi), (2 * .pi)) - .pi

		return CLLocationCoordinate2D(latitude: CLLocationDegrees(fromRadians: toLatRadians),
									  longitude: CLLocationDegrees(fromRadians: toLonRadians))
	}
}

extension CLLocationDegrees {
	func radians() -> Double {
		self * .pi / 180.0
	}

	init(fromRadians radians: Double) {
		self = radians * 180.0 / .pi
	}
}
