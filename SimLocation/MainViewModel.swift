//
//  MainViewModel.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-20.
//

import Foundation
import MapKit
import CoreLocation
import Combine

// MARK: - Simulators

private struct Simulators: Codable {
	private let devices: [String: [Simulator]]

	static func booted(completion: @escaping ([UUID]) -> Void) {
		DispatchQueue.global(qos: .utility).async {
			let task = Process()
			task.launchPath = "/usr/bin/xcrun"
			task.arguments = ["simctl", "list", "-j", "devices"]

			let pipe = Pipe()
			task.standardOutput = pipe

			task.launch()

			let data = pipe.fileHandleForReading.readDataToEndOfFile()
			task.waitUntilExit()
			pipe.fileHandleForReading.closeFile()

			guard task.terminationStatus == 0 else {
				completion([])
				return
			}

			guard let simulators = try? JSONDecoder().decode(Simulators.self, from: data) else {
				completion([])
				return
			}

			let uuids =	simulators.devices
				.reduce(into: [Simulator](), { partialResult, keyAndValue in
					partialResult.append(contentsOf: keyAndValue.value)
				})
				.filter { $0.isBooted }
				.map { $0.udid }
			completion(uuids)
		}
	}
}

private struct Simulator: Codable {
	private let state: String
	let udid: UUID

	var isBooted: Bool {
		return self.state == "Booted"
	}
}

// MARK: - Model

struct SavedLocations: Codable {
	var locations: [SavedLocation]
}

struct SavedLocation: Identifiable, Hashable, Codable {
	let id: UUID
	var name: String
	let location: CLLocationCoordinate2D

	func hash(into hasher: inout Hasher) {
		hasher.combine(self.id)
	}
}

extension SavedLocations {
	init?(data: Data?) throws {
		guard let data else { return nil }
		self = try JSONDecoder().decode(Self.self, from: data)
	}

	func toData() throws -> Data {
		try JSONEncoder().encode(self)
	}
}

// MARK: - ViewModel

class MainViewModel: ObservableObject {
	// used for publishing region changes from ViewModel up to the View.
	// does not receive updates _from_ the View
	// The View doesn't bind to this publisher because it makes View updates really slow :-/
	@Published var region: MKCoordinateRegion

	// the collection of saved locations, shown in the sidebar
	@Published var savedLocations: SavedLocations {
		didSet {
			UserDefaults.standard.locations = savedLocations
		}
	}

	// this is the saved location where we're currently located
	@Published var savedSelection: SavedLocation?
	
	@Published var jitter: Bool

	// updates received from the View go here, processed on
	// a background queue
	private var regionUpdates: CurrentValueSubject<MKCoordinateRegion, Never>

	private var cancellables = Set<AnyCancellable>()
	private var timerCancellable: AnyCancellable?
	private var simulators = [UUID]()

	init() {
		UserDefaults.registerSimLocationDefaults()

		self.region = UserDefaults.standard.region
		self.regionUpdates = CurrentValueSubject(UserDefaults.standard.region)
		self.savedLocations = UserDefaults.standard.locations
		self.jitter = UserDefaults.standard.jitter

		// watch for region changes, and broadcast using DistributedNotificationCenter
		// at most once per second
		self.regionUpdates
			.collect(.byTime(DispatchQueue.global(qos: .utility), 1))
			.compactMap(\.last)
			.sink { region in
				self.broadcast(region.center)
				UserDefaults.standard.region = region
			}
			.store(in: &self.cancellables)

		// when the user selects a saved location, update the location on the map
		self.$savedSelection
			.receive(on: DispatchQueue.main)
			.sink { savedLocation in
				guard let savedLocation else { return }

				self.region = MKCoordinateRegion(center: savedLocation.location,
												 span: self.regionUpdates.value.span)
			}
			.store(in: &self.cancellables)

		self.$jitter
			.sink { jitter in
				UserDefaults.standard.jitter = jitter
				
				if jitter {
					self.timerCancellable = Timer.publish(every: 2,
														  on: .main,
														  in: .default)
					.autoconnect()
					.receive(on: DispatchQueue.main)
					.sink { _ in
						let latestLocation = self.regionUpdates.value.center
						let location = latestLocation.adjusted(distance: Double.random(in: 2..<5),
															   bearingRadians: Double.random(in: 0..<(2 * .pi)))
						self.broadcast(location)
					}
				} else {
					self.timerCancellable = nil
				}
			}
			.store(in: &self.cancellables)
	}

	private func broadcast(_ coordinate: CLLocationCoordinate2D) {
		let userInfo: [AnyHashable: Any] = [
				"simulateLocationLatitude": coordinate.latitude,
				"simulateLocationLongitude": coordinate.longitude,
				"simulateLocationDevices": self.simulators.map(\.uuidString),
			]

			let notification = Notification(name: Notification.Name(rawValue: "com.apple.iphonesimulator.simulateLocation"), object: nil,
											userInfo: userInfo)

		DistributedNotificationCenter.default()
			.post(notification)
	}

	func onAppear() {
		self.findSims()
	}

	func onRegionChange(_ region: MKCoordinateRegion) {
		DispatchQueue.global(qos: .utility).async {
			self.regionUpdates.value = region
		}
	}

	func zoomIn() {
		let currentRegion = regionUpdates.value
		let newSpan = MKCoordinateSpan(latitudeDelta: currentRegion.span.latitudeDelta * 0.5,
									   longitudeDelta: currentRegion.span.longitudeDelta * 0.5)
		self.region = MKCoordinateRegion(center: currentRegion.center,
										 span: newSpan)
	}

	func zoomOut() {
		let currentRegion = regionUpdates.value
		let newSpan = MKCoordinateSpan(latitudeDelta: currentRegion.span.latitudeDelta * 2.0,
									   longitudeDelta: currentRegion.span.longitudeDelta * 2.0)
		self.region = MKCoordinateRegion(center: currentRegion.center,
										 span: newSpan)
	}

	func save() {
		let currentRegion = regionUpdates.value
		let location = SavedLocation(id: UUID(),
									 name: "Location \(self.savedLocations.locations.count + 1)",
									 location: currentRegion.center)

		var locations = self.savedLocations.locations
		locations.append(location)
		self.savedLocations = SavedLocations(locations: locations)
	}

	func findSims() {
		Simulators.booted { simulators in
			self.simulators = simulators

			let currentRegion = self.regionUpdates.value
			self.broadcast(currentRegion.center)
		}
	}
}
