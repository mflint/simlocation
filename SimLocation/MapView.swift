//
//  MapView.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-21.
//

import SwiftUI
import MapKit

struct MapView: View {
	@ObservedObject var viewModel: MainViewModel
	@State var region: MKCoordinateRegion

	init(viewModel: MainViewModel) {
		self.viewModel = viewModel
		self.region = viewModel.region
	}

	var body: some View {
		ZStack {
			Map(coordinateRegion: self.$region)
				.animation(.default,
						   value: self.region)

			Circle()
				.strokeBorder(lineWidth: 3)
				.frame(width: 16, height: 16)
				.foregroundColor(.red)
				.offset(y: -26) // ¯\_(ツ)_/¯
				.disabled(true)
		}
		.overlay(alignment: .topTrailing, content: {
				ControlGroup {
					Button {
						self.viewModel.zoomIn()
					} label: {
						Label("Zoom in", systemImage: "plus")
					}
					.help("+")

					Button {
						self.viewModel.zoomOut()
					} label: {
						Label("Zoom out", systemImage: "minus")
					}
					.help("-")
				}
				.frame(maxWidth: 75)
				.padding()
		})
		.onChange(of: self.region) { region in
			// when the user pans the map, update the ViewModel
			self.viewModel.onRegionChange(region)
		}
		.onChange(of: self.viewModel.region) { newValue in
			// when the ViewModel changes, update the View
			if newValue != self.region {
				self.region = newValue
			}
		}
	}
}
