//
//  SimLocationApp.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-20.
//

import SwiftUI

@main
struct SimLocationApp: App {
	var body: some Scene {
		WindowGroup {
			MainView()
		}
	}
}
