//
//  SavedLocationsListView.swift
//  SimLocation
//
//  Created by Matthew Flint on 2023-07-21.
//

import SwiftUI

struct SavedLocationsListView: View {
	@ObservedObject var viewModel: MainViewModel

	var body: some View {
		List(selection: self.$viewModel.savedSelection) {
			ForEach(self.$viewModel.savedLocations.locations) { location in
				NavigationLink(value: location.wrappedValue) {
					SavedLocationCell(locations: self.$viewModel.savedLocations,
									  location: location)
				}
			}
			.onMove { from, to in
				self.viewModel.savedLocations.locations.move(fromOffsets: from, toOffset: to)
			}
		}
		.keyStroke(key: .delete, modifiers: []) {
			self.viewModel.savedLocations.locations.removeAll {
				$0.id == self.viewModel.savedSelection?.id
			}
		}
	}
}

private struct SavedLocationCell: View {
	@Binding var locations: SavedLocations
	@Binding var location: SavedLocation
	@FocusState private var isFocused: Bool

	var body: some View {
		TextField(text: self.$location.name) {
			Text(self.location.name)
		}
		.focused($isFocused)
		.contextMenu {
			RenameButton()
				.renameAction {
					isFocused = true
				}
			Button("Delete") {
				self.locations.locations.removeAll {
					$0.id == location.id
				}
			}
		}
	}
}
