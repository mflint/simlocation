# SimLocation

A macOS app for setting the simulated location in an iOS simulator.

![Screenshot](screenshot.png)

## How to use

Drag the map to set the simulated location.

The app discovers booted simulators when it starts. To discover them again, press the _Find sims_ button.

The _Save location_ button saves the current location in the sidebar. These stored locations and the current map location are persisted in UserDefaults.

You can rename or delete saved locations by right-clicking (or control-clicking) on the location name in the sidebar, and swich between saved locations using up/down arrow keys.

Shortcuts:
* ⌘S save current location
* ⌘F find simulators
* ⌘J enable/disable jitter mode
* +/- zoom in/out
* up/down: select the previous/next saved location
* delete: delete the selected saved location
